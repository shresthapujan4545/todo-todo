import { initializeApp } from "firebase";

const app = initializeApp({
  apiKey: "AIzaSyA1EsX-QFaRil_H64e4ABtOJ1ZbYmS3eQY",
  authDomain: "vuetrainship.firebaseapp.com",
  databaseURL: "https://vuetrainship.firebaseio.com",
  projectId: "vuetrainship",
  storageBucket: "vuetrainship.appspot.com",
  messagingSenderId: "403232230839",
  appId: "1:403232230839:web:acb370fa64b693c039ac32",
  measurementId: "G-RWMGF5T8B7"
});

export const db = app.database();
export const namesRef = db.ref("names");
