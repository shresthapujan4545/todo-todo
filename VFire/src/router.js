import Vue from "vue";
import Router from "vue-router";
import Toto from "./Toto";
import About from "./About";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "toto",
      component: Toto
    },
    {
      path: "/about",
      name: "about",
      component: About
    }
  ]
});
