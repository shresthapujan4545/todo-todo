import "./firebase";
import Vue from "vue";
import App from "./App.vue";
import { rtdbPlugin } from "vuefire";
import router from "./router";

Vue.use(rtdbPlugin);

new Vue({
  router,
  el: "#app",
  render: h => h(App)
});
